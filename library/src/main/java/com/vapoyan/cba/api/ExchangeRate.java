package com.vapoyan.cba.api;

import org.simpleframework.xml.Element;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 *
 */
@SuppressWarnings("unused")
@Accessors(prefix = "m")
public class ExchangeRate {

    @Getter
    @Element(name = "ISO")
    private String mISO;

    @Getter
    @Element(name = "Amount", required = false)
    private Integer mAmount;

    @Getter
    @Element(name = "Rate")
    private Double mRate;

    @Getter
    @Element(name = "Difference", required = false)
    private String mDifference;

    public ExchangeRate() {

    }

    public ExchangeRate(String iso, Double rate) {
        mISO = iso;
        mRate = rate;
    }
}
