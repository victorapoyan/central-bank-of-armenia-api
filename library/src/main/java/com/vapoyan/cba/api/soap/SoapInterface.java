/*
 * Copyright (C) 2016. Victor Apoyan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vapoyan.cba.api.soap;

import com.vapoyan.cba.api.soap.models.response.ERByDateResponse;
import com.vapoyan.cba.api.soap.models.request.ERByDateByISORequest;
import com.vapoyan.cba.api.soap.models.response.ERByDateByISOResponse;
import com.vapoyan.cba.api.soap.models.request.ERByDateRequest;
import com.vapoyan.cba.api.soap.models.request.ERLByISORequest;
import com.vapoyan.cba.api.soap.models.response.ERLByISOResponse;
import com.vapoyan.cba.api.soap.models.request.ERLRequest;
import com.vapoyan.cba.api.soap.models.response.ERLResponse;
import com.vapoyan.cba.api.soap.models.request.ISOCodesRequest;
import com.vapoyan.cba.api.soap.models.response.ISOCodesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 *
 */
interface SoapInterface {

    /**
     * Retrieves the latest published rates. Includes trend.
     * Recommended for Web sites integration.
     */
    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("exchangerates.asmx")
    Call<ERLResponse> exchangeRatesLatest(@Body ERLRequest request);

    /**
     * Retrieves all  available rates of the specified date.
     * Includes trend.
     */
    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("exchangerates.asmx")
    Call<ERByDateResponse> exchangeRatesByDate(@Body ERByDateRequest request);

    /**
     * Retrieves latest rates of the specified date and ISO,
     * will return single line. Includes trend. Check ISO
     * Codes method for all available codes.
     */
    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("exchangerates.asmx")
    Call<ERByDateByISOResponse> exchangeRatesByDateByISO(@Body ERByDateByISORequest request);

// TODO: To be Implemented ..
//    /**
//     * Retrieves rates  for  the  specified period for comma
//     * separated ISO codes list.
//     */
//    @Headers({
//            "Content-Type: application/soap+xml",
//            "Accept-Charset: utf-8"
//    })
//    @POST("exchangerates.asmx")
//    ExchangeRatesByDateRangeByISOResponse exchangeRatesByDateRangeByISO(@Body ExchangeRatesByDateRangeByISORequest request);

    /**
     * Retrieves  latest  rates  of  the specified ISO, will
     * return single line.  Includes  trend. Check ISO Codes
     * method for all available codes.
     */
    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("exchangerates.asmx")
    Call<ERLByISOResponse> exchangeRatesLatestByISO(@Body ERLByISORequest request);

    /**
     * List of the latest available ISO codes. Based on
     * latest published rates.
     */
    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("exchangerates.asmx")
    Call<ISOCodesResponse> isoCodes(@Body ISOCodesRequest request);

// TODO: To be Implemented
//    /**
//     * Detailed list of codes.
//     */
//    @Headers({
//            "Content-Type: application/soap+xml",
//            "Accept-Charset: utf-8"
//    })
//    @POST("exchangerates.asmx")
//    Call<ISOCodesDetailedResponse> isoCodesDetailed(@Body ISOCodesDetailedRequest request);
}