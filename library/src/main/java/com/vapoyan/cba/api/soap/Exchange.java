/*
 * Copyright (C) 2016. Victor Apoyan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vapoyan.cba.api.soap;



import com.vapoyan.cba.api.soap.callbacks.ExchangeCallback;
import com.vapoyan.cba.api.soap.models.request.ERByDateByISORequest;
import com.vapoyan.cba.api.soap.models.request.ERByDateRequest;
import com.vapoyan.cba.api.soap.models.request.ERLByISORequest;
import com.vapoyan.cba.api.soap.models.request.ERLRequest;
import com.vapoyan.cba.api.soap.models.request.ISOCodesRequest;
import com.vapoyan.cba.api.soap.models.response.ERByDateByISOResponse;
import com.vapoyan.cba.api.soap.models.response.ERByDateResponse;
import com.vapoyan.cba.api.soap.models.response.ERLByISOResponse;
import com.vapoyan.cba.api.soap.models.response.ERLResponse;
import com.vapoyan.cba.api.soap.models.response.ISOCodesResponse;


import java.util.concurrent.TimeUnit;

import lombok.NonNull;
import okhttp3.OkHttpClient;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */
public final class Exchange {

    private final SoapClient mSoapClient;

    public static class Builder {
        private int mReadTimeout = 10_000;
        private int mWriteTimeout = 10_000;
        private int mConnectTimeout = 10_000;

        public Builder setReadTimeout(int readTimeout){
            mReadTimeout = readTimeout; return this;
        }

        public Builder setWriteTimeout(int writeTimeout){
            mWriteTimeout = writeTimeout; return this;
        }

        public Builder setConnectTimeout(int connectTimeout){
            mConnectTimeout = connectTimeout; return this;
        }

        public Exchange build() {
            return new Exchange(this);
        }
    }

    private Exchange(Builder builder) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .readTimeout(builder.mReadTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(builder.mWriteTimeout, TimeUnit.MILLISECONDS)
                .connectTimeout(builder.mConnectTimeout, TimeUnit.MILLISECONDS)
                .build();
        mSoapClient = new SoapClient(client);
    }

    public Exchange() {
        OkHttpClient client = new OkHttpClient();
        mSoapClient = new SoapClient(client);
    }


    /**
     * Asynchronously send Latest Rates request and notify {@code callback}
     * of its response or if an error occurred talking to the server, creating
     * the request, or processing the response.
     */
    void getLatestRates(@NonNull final ExchangeCallback<ERLResponse> callback) {
        mSoapClient.getSoap().exchangeRatesLatest(new ERLRequest())
        .enqueue(new Callback<ERLResponse>() {
            @Override
            public void onResponse(Response<ERLResponse> response) {
                final ERLResponse body = response.body();
                if (body != null)
                    callback.onResponse(body);
                else callback.onFailure(new Throwable(response.message()));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    /**
     * Asynchronously send Latest Rates request for given ISO code and notify
     * {@code callback} of its response or if an error occurred talking to the
     * server, creating the request, or processing the response.
     *
     * @param iso ISO code for which request must be done.
     */
    void getLatestRates(@NonNull final String iso, @NonNull final ExchangeCallback<ERLByISOResponse> callback) {
        mSoapClient.getSoap().exchangeRatesLatestByISO(new ERLByISORequest(iso))
        .enqueue(new Callback<ERLByISOResponse>() {
            @Override
            public void onResponse(Response<ERLByISOResponse> response) {
                final ERLByISOResponse body = response.body();
                if (body != null)
                    callback.onResponse(body);
                else callback.onFailure(new Throwable(response.message()));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    /**
     * Asynchronously send Rates request for given Date and notify
     * {@code callback} of its response or if an error occurred talking to the
     * server, creating the request, or processing the response.
     *
     * @param date Date for which request must be done.
     *             Format: yyyy-MM-ddTHH:mm:ss
     *             Example: 2016-02-07T00:00:00
     */
    void getRates(@NonNull final String date, @NonNull final ExchangeCallback<ERByDateResponse> callback) {
        mSoapClient.getSoap().exchangeRatesByDate(new ERByDateRequest(date))
        .enqueue(new Callback<ERByDateResponse>() {
            @Override
            public void onResponse(Response<ERByDateResponse> response) {
                final ERByDateResponse body = response.body();
                if (body != null)
                    callback.onResponse(body);
                else callback.onFailure(new Throwable(response.message()));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    /**
     * Asynchronously send Rates request for given Date and ISO code and notify
     * {@code callback} of its response or if an error occurred talking to the
     * server, creating the request, or processing the response.
     *
     * @param data Data for which information must be requested.
     *             Format: yyyy-MM-ddTHH:mm:ss
     *             Example: 2016-02-07T00:00:00
     * @param iso ISO code for which information must be requested.
     */
    void getRates(@NonNull final String data, @NonNull final String iso, @NonNull final ExchangeCallback<ERByDateByISOResponse> callback) {
        mSoapClient.getSoap().exchangeRatesByDateByISO(new ERByDateByISORequest(data, iso))
        .enqueue(new Callback<ERByDateByISOResponse>() {
            @Override
            public void onResponse(Response<ERByDateByISOResponse> response) {
                final ERByDateByISOResponse body = response.body();
                if (body != null)
                    callback.onResponse(body);
                else callback.onFailure(new Throwable(response.message()));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    /**
     * Asynchronously send IOS codes request for and notify
     * {@code callback} of its response or if an error occurred talking to the
     * server, creating the request, or processing the response.
     */
    void getIsoCodes(@NonNull final ExchangeCallback<ISOCodesResponse> callback) {
        mSoapClient.getSoap().isoCodes(new ISOCodesRequest())
        .enqueue(new Callback<ISOCodesResponse>() {
            @Override
            public void onResponse(Response<ISOCodesResponse> response) {
                final ISOCodesResponse body = response.body();
                if (body != null)
                    callback.onResponse(body);
                else callback.onFailure(new Throwable(response.message()));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}