package com.vapoyan.cba.api.soap.models.response;

import com.vapoyan.cba.api.ExchangeRate;

import org.simpleframework.xml.ElementList;

import java.util.List;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 *
 */
@SuppressWarnings("unused")
@Accessors(prefix = "m")
class Rates {
    @Getter
    @ElementList(entry = "ExchangeRate", inline = true)
    private List<ExchangeRate> mExchangeRates;
}
