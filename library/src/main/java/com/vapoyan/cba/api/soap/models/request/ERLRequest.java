/*
 * Copyright (C) 2016. Victor Apoyan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vapoyan.cba.api.soap.models.request;

/*
 * POST /exchangerates.asmx HTTP/1.1
 * Host: api.cba.am
 * Content-Type: application/soap+xml; charset=utf-8
 * Content-Length: length
 *
 * <?xml version="1.0" encoding="utf-8"?>
 * <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 *                  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 *                  xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
 *   <soap12:Body>
 *     <ExchangeRatesLatest xmlns="http://www.cba.am/" />
 *   </soap12:Body>
 * </soap12:Envelope>
 */

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/**
 *
 */
@SuppressWarnings("unused")
@Root(name = "soap12:Envelope")
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
        @Namespace(prefix = "soap12", reference = "http://www.w3.org/2003/05/soap-envelope")
})
public final class ERLRequest {
    @Element(name = "soap12:Body")
    private ExchangeRatesLatestRequestBody mBody = new ExchangeRatesLatestRequestBody();

    private static class ExchangeRatesLatestRequestBody {
        @Element(name = "ExchangeRatesLatest")
        private ExchangeRatesLatest mExchangeRatesLatest = new ExchangeRatesLatest();
    }

    private static class ExchangeRatesLatest {
        @Attribute(name = "xmlns")
        private String mXmlns = "http://www.cba.am/";
    }
}