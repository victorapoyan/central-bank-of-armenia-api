/*
 * Copyright (C) 2016. Victor Apoyan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vapoyan.cba.api.soap.models.response;

import com.vapoyan.cba.api.ExchangeRate;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import lombok.experimental.Accessors;

/*
 * HTTP/1.1 200 OK
 * Content-Type: application/soap+xml; charset=utf-8
 * Content-Length: length
 *
 * <?xml version="1.0" encoding="utf-8"?>
 * <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 *                  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 *                  xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
 *   <soap12:Body>
 *     <ExchangeRatesLatestResponse xmlns="http://www.cba.am/">
 *       <ExchangeRatesLatestResult>
 *         <CurrentDate>dateTime</CurrentDate>
 *         <NextAvailableDate>dateTime</NextAvailableDate>
 *         <PreviousAvailableDate>dateTime</PreviousAvailableDate>
 *         <Rates>
 *           <ExchangeRate>
 *             <ISO>string</ISO>
 *             <Amount>string</Amount>
 *             <Rate>string</Rate>
 *             <Difference>string</Difference>
 *           </ExchangeRate>
 *           <ExchangeRate>
 *             <ISO>string</ISO>
 *             <Amount>string</Amount>
 *             <Rate>string</Rate>
 *             <Difference>string</Difference>
 *           </ExchangeRate>
 *         </Rates>
 *       </ExchangeRatesLatestResult>
 *     </ExchangeRatesLatestResponse>
 *   </soap12:Body>
 * </soap12:Envelope>
 */

/**
 *
 */
@SuppressWarnings("unused")
@Root(name = "soap12:Envelope")
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
        @Namespace(prefix = "soap12", reference = "http://www.w3.org/2003/05/soap-envelope")
})
public class ERLResponse {

    private final static DateFormat FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);

    @Element(name = "Body")
    @Namespace(prefix = "soap12")
    private Body mBody;

    @Accessors(prefix = "m")
    static class Body {
        @Getter
        @Element(name = "ExchangeRatesLatestResponse")
        private Response mResponse;
    }

    @Accessors(prefix = "m")
    static class Response {
        @Attribute(name = "xmlns", required = false)
        private String mXmlns;

        @Getter
        @Element(name = "ExchangeRatesLatestResult")
        private Result mResult;
    }

    // PUBLIC METHODS
    /////////////////////////////////////////////////

    /**
     * This function get {@link ExchangeRate} for given currency.
     * @return {@link ExchangeRate} for given currency; null if error occurs.
     */
    public List<ExchangeRate> getExchangeRate() {
        return new ArrayList<>(mBody.getResponse().getResult().getRates().getExchangeRates());
    }

    /**
     * This function returns Current Date when latest Rates were updated.
     * @return Current Date when latest Rates were updated.
     */
    public Date getCurrentDate() {
        final String date = mBody.getResponse().getResult().getCurrentDate();
        try { return FORMAT.parse(date); }
        catch (ParseException e) { return null; }
    }

    /**
     * This function returns next Date when latest Rates will be updated
     * @return Next Date when latest Rates will be updated.
     */
    public Date getNextAvailableDate() {
        final String date = mBody.getResponse().getResult().getNextAvailableDate();
        try { return FORMAT.parse(date); }
        catch (ParseException e) { return null; }
    }

    /**
     * This function returns previous Date when latest Rates were updated.
     * @return Previous Date when latest Rates were updated.
     */
    public Date getPreviousAvailableDate() {
        final String date = mBody.getResponse().getResult().getPreviousAvailableDate();
        try { return FORMAT.parse(date); }
        catch (ParseException e) { return null; }
    }
}