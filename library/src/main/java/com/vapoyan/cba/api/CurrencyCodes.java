package com.vapoyan.cba.api;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 *
 */
@Accessors(prefix = "m")
public enum CurrencyCodes {

    USD("USD"),
    GBP("GBP"),
    AUD("AUD"),
    ATS("ATS"),
    ARS("ARS"),
    BEF("BEF"),
    DEM("DEM"),
    DKK("DKK"),
    EGP("EGP"),
    EUR("EUR"),
    SDR("SDR"),
    TRY("TRY"),
    IEP("IEP"),
    IRR("IRR"),
    ESP("ESP"),
    ILS("ILS"),
    ITL("ITL"),
    PLN("PLN"),
    LBP("LBP"),
    CAD("CAD"),
    INR("INR"),
    NLG("NLG"),
    GRD("GRD"),
    HUF("HUF"),
    JPY("JPY"),
    NOK("NOK"),
    SEK("SEK"),
    CHF("CHF"),
    CZK("CZK"),
    CNY("CNY"),
    PTE("PTE"),
    SGD("SGD"),
    SKK("SKK"),
    FIM("FIM"),
    FRF("FRF"),
    KRW("KRW"),
    BRL("BRL"),
    MXN("MXN"),
    SAR("SAR"),
    SYP("SYP"),
    AED("AED"),
    KWD("KWD"),
    BGN("BGN"),
    RON("RON"),
    ISK("ISK"),
    LVL("LVL"),
    LTL("LTL"),
    KGS("KGS"),
    KZT("KZT"),
    MDL("MDL"),
    RUB("RUB"),
    UAH("UAH"),
    UZS("UZS"),
    BYR("BYR"),
    TJS("TJS"),
    TMT("TMT"),
    GEL("GEL"),
    HKD("HKD");

    private final String mCode;

    CurrencyCodes(final String code) {
        mCode = code;
    }

    @Override
    public String toString() {
        return mCode;
    }
}
