package com.vapoyan.cba.api.soap.models.response;

import org.simpleframework.xml.Element;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 *
 */
@SuppressWarnings("unused")
@Accessors(prefix = "m")
class Result {
    @Getter
    @Element(name = "CurrentDate", required = false)
    private String mCurrentDate;
    @Getter
    @Element(name = "NextAvailableDate", required = false)
    private String mNextAvailableDate;
    @Getter
    @Element(name = "PreviousAvailableDate", required = false)
    private String mPreviousAvailableDate;

    @Getter
    @Element(name = "Rates")
    private Rates mRates;
}