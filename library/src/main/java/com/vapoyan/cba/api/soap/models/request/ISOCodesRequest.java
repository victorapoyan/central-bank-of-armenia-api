/*
 * Copyright (C) 2016. Victor Apoyan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vapoyan.cba.api.soap.models.request;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/*
 * POST /exchangerates.asmx HTTP/1.1
 * Host: api.cba.am
 * Content-Type: application/soap+xml; charset=utf-8
 * Content-Length: length
 *
 * <?xml version="1.0" encoding="utf-8"?>
 * <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 *                  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 *                  xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
 *   <soap12:Body>
 *     <ISOCodes xmlns="http://www.cba.am/" />
 *   </soap12:Body>
 * </soap12:Envelope>
 */

/**
 *
 */
@SuppressWarnings("unused")
@Root(name = "soap12:Envelope")
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
        @Namespace(prefix = "soap12", reference = "http://www.w3.org/2003/05/soap-envelope")
})
public final class ISOCodesRequest {
    @Element(name = "soap12:Body")
    private Body mBody = new Body();

    private static class Body {
        @Element(name = "ISOCodes")
        private Request mISOCodes = new Request();
    }

    private static class Request {
        @Attribute(name = "xmlns")
        private String mXmlns = "http://www.cba.am/";
    }
}