/*
 * Copyright (C) 2016. Victor Apoyan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vapoyan.cba.api.soap.models.response;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import java.util.List;

import lombok.Getter;
import lombok.experimental.Accessors;

/*
 * HTTP/1.1 200 OK
 * Content-Type: application/soap+xml; charset=utf-8
 * Content-Length: length
 *
 * <?xml version="1.0" encoding="utf-8"?>
 * <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
 *  <soap12:Body>
 *     <ISOCodesResponse xmlns="http://www.cba.am/">
 *       <ISOCodesResult>
 *         <string>string</string>
 *         <string>string</string>
 *       </ISOCodesResult>
 *     </ISOCodesResponse>
 *   </soap12:Body>
 * </soap12:Envelope>
 */

/**
 *
 */
@SuppressWarnings("unused")
@Root(name = "soap12:Envelope")
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
        @Namespace(prefix = "soap12", reference = "http://www.w3.org/2003/05/soap-envelope")
})
public final class ISOCodesResponse {
    @Element(name = "Body")
    @Namespace(prefix = "soap12")
    private Body mBody;

    public List<String> getIsoCodes() {
        return mBody.getISOCodeResponse().getISOCodesResult().getISOCodes();
    }

    @Accessors(prefix = "m")
    private static class Body {
        @Getter
        @Element(name = "ISOCodesResponse")
        private Response  mISOCodeResponse ;
    }

    @Accessors(prefix = "m")
    private static class Response {
        @Attribute(name = "xmlns", required = false)
        private String mXmlns;

        @Getter
        @Element(name = "ISOCodesResult")
        private Result mISOCodesResult;
    }

    @Accessors(prefix = "m")
    private static class Result {
        @Getter
        @ElementList(entry = "string", inline = true)
        private List<String> mISOCodes;
    }

}